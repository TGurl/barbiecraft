# BarbieCraft Notes

## What will this pack be about?
I don't know myself. This pack is mainly meant to learn how to create a modpack. Not just a
kitchen-sink pack, but a real pack with scripts and more.

## Minecraft Version
This pack will be for Minecraft version 1.12.2. Mainly because the most mods are available vor
1.12.2

## Base mods
- Backups
- Controlling
- FTB Libs
- FTB Utilities
- InventoryTweaks
- JustEnoughIDs
- Just Enough Items
- Just Enough Resources
- MouseTweaks
- Placebo
- StepUp
- ToastControl

